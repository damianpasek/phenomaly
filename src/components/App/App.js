import React, { Component } from 'react';
import Counter from "../Counter/Counter";
import FileUpload from "../FileUpload/FileUpload";
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="container">
        <Counter/>
        <FileUpload/>
      </div>
    );
  }
}

export default App;
