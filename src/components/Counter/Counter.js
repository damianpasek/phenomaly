import React from "react";
import classNames from 'classnames';
import './Counter.css';

class Counter extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      count: 0
    }
  }

  isCounterDivisibleBy(number) {
    const {count} = this.state;
    return count && count % number === 0;
  }

  getLabelText() {
    if (this.isCounterDivisibleBy(3) && this.isCounterDivisibleBy(5)) {
      return 'FizzBuzz';
    } else if (this.isCounterDivisibleBy(3)) {
      return 'Fizz';
    } else if (this.isCounterDivisibleBy(5)) {
      return 'Buzz';
    }
    return this.state.count;
  }

  render() {
    const textClass = classNames({
      'text': true,
      'text-yellow': this.isCounterDivisibleBy(3) && !this.isCounterDivisibleBy(5),
      'text-blue': this.isCounterDivisibleBy(5) && !this.isCounterDivisibleBy(3),
      'text-green': this.isCounterDivisibleBy(3) && this.isCounterDivisibleBy(5)
    });

    return (
      <div className="component-counter row align-items-center">
        <div className="col-md-12">
          <h2>Counter</h2>
        </div>
        <div className="col-md-3">
          <button
            id="count-button"
            onClick={() => this.setState({count: this.state.count + 1})}
            className="btn btn-default">+1</button>
        </div>
        <div className="col-md-9">
          <div className={textClass}>{this.getLabelText()}</div>
        </div>
      </div>
    )
  }

}

export default Counter;
