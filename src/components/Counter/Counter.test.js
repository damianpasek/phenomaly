import React from 'react';
import { shallow } from 'enzyme';
import Counter from './Counter';
import 'jest-enzyme';


describe('Counter Component', () => {

  it('Counter component for initial state', () => {
    const counter = shallow(<Counter/>);

    expect(counter.find('div.text')).not.toHaveClassName('text-yellow');
    expect(counter.find('div.text')).not.toHaveClassName('text-blue');
    expect(counter.find('div.text')).not.toHaveClassName('text-green');
  });

  it('Counter component for count of 3', () => {
    const counter = shallow(<Counter/>);
    counter.setState({count: 3});

    expect(counter.find('div.text')).toHaveText('Fizz');
    expect(counter.find('div.text')).toHaveClassName('text-yellow');
    expect(counter.find('div.text')).not.toHaveClassName('text-blue');
    expect(counter.find('div.text')).not.toHaveClassName('text-green');
  });

  it('Counter component for count of 5', () => {
    const counter = shallow(<Counter/>);
    counter.setState({count: 5});

    expect(counter.find('div.text')).toHaveText('Buzz');
    expect(counter.find('div.text')).not.toHaveClassName('text-yellow');
    expect(counter.find('div.text')).toHaveClassName('text-blue');
    expect(counter.find('div.text')).not.toHaveClassName('text-green');
  });

  it('Counter component for count of 15', () => {
    const counter = shallow(<Counter/>);
    counter.setState({count: 15});

    expect(counter.find('div.text')).toHaveText('FizzBuzz');
    expect(counter.find('div.text')).not.toHaveClassName('text-yellow');
    expect(counter.find('div.text')).not.toHaveClassName('text-blue');
    expect(counter.find('div.text')).toHaveClassName('text-green');
  });

  it('Simulate button click', () => {
    const counter = shallow(<Counter/>);

    expect(counter).toHaveState('count', 0);

    counter.find('button#count-button').simulate('click');
    expect(counter).toHaveState('count', 1);

    counter.find('button#count-button').simulate('click');
    expect(counter).toHaveState('count', 2);
  });
});
