import React from 'react';
import {round} from 'lodash';
import './FileUpload.css';

class FileUpload extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      file: null
    };

    this.handleFileChange = this.handleFileChange.bind(this);
  }

  isFileImage(file) {
    return ["image/gif", "image/jpeg", "image/png"].includes(file.type);
  }

  getFileSize(file) {
    const {size} = file;

    if (size > (1024 ** 2)) {
      return `${round(size / (1024 ** 2), 1)} MB`;
    }
    return `${round(size / 1024, 1)} KB`;
  }

  setImageInfo(file) {
    const img = new Image();
    img.onload = (event) => this.setState({imgHeight: event.target.height, imgWidth: event.target.width});
    img.src = window.URL.createObjectURL(file);
    return img;
  }

  handleFileChange(event) {
    this.setState({file: event.target.files[0]});
  }

  renderFileForm() {
    const {file} = this.state;

    return (
      <div className="col-md-12 file-form">
        <div className="input-group">
          <input type="text" className="form-control" value={file ? file.name : ''} readOnly={true} />
          <label className="input-group-btn">
              <span className="btn btn-danger">
                Select a file
                <input type="file" onChange={this.handleFileChange} />
              </span>
          </label>
        </div>
      </div>
    )
  }

  renderImageInfo() {
    const {file, imgHeight, imgWidth} = this.state;
    const img = this.setImageInfo(file);

    return (
      <div className="row">
        <div className="col-md-5">
          <img src={img.src} alt=""/>
        </div>
        <div className="col-md-7">
          <ul className="list-group">
            <li className="list-group-item">Name: {file.name}</li>
            <li className="list-group-item">Width: {imgHeight}</li>
            <li className="list-group-item">Height: {imgWidth}</li>
            <li className="list-group-item">Size: {this.getFileSize(file)}</li>
          </ul>
        </div>
      </div>
    )
  }

  renderOtherFileInfo() {
    const {file} = this.state;

    return (
      <ul className="list-group">
        <li className="list-group-item">Type: {file.type}</li>
        <li className="list-group-item">Size: {this.getFileSize(file)}</li>
      </ul>
    )
  }

  renderFileInfo() {
    const {file} = this.state;

    if (file) {
      return (
        <div className="col-md-12 file-info">
          <div className="card">
            <div className="card-block">
            <pre>
              {this.isFileImage(file) ? this.renderImageInfo() : this.renderOtherFileInfo()}
            </pre>
            </div>
          </div>
        </div>
      )
    }
    return <div/>;
  }

  render() {
    return (
      <div className="component-file-upload row">
        {this.renderFileForm()}
        {this.renderFileInfo()}
      </div>
    )
  }
}

export default FileUpload;
